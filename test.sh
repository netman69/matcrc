#!/bin/sh

checkresult() {
	if [ "$result" == "$expect" ]; then
		echo "  Result $result is correct."
	else
		echo "  ERROR!"
		echo "  Expected $expect, got $result."
	fi
}

# Test 1
echo "Running test 1"
echo -n 123456789 > test.txt
expect="E3069283"
result=`./crc32c test.txt`
checkresult
rm test.txt

# Test 2
echo "Running test 2"
expect="22620404"
result=`echo -n "The quick brown fox jumps over the lazy dog" | ./crc32c`
checkresult

# Test 3
echo "Running test 3 (piping 1G bytes from /dev/zero)"
expect="3984F745"
result=`head -c 1000000000 /dev/zero | ./crc32c`
checkresult

# Test 4
echo "Running test 3 (piping 10G bytes from /dev/zero)"
expect="C4B0A9B1"
result=`head -c 10000000000 /dev/zero | ./crc32c`
checkresult

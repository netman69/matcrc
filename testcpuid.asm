;
; SSE4.2 capable command line CRC32C calculation utility for Linux and FreeBSD.
;
; Usage:
;   crc32c [<filename>]
;

; Buffer may be not TOO tiny cause output string is also written there.
%define BUFSIZE 4096

bits 64

%ifndef OS
	%fatal "Please specify operating system."
%endif

%if OS == "Linux"
	%define sys_read 0x00
	%define sys_write 0x01
	%define sys_open 0x02
	%define sys_close 0x03
	%define sys_exit 0x3C

	%macro init 0 ; Place arguments { argc, arg0, argv1, ... } in rbp.
		mov rbp, rsp
		add rbp, 8
	%endm

	%macro checkerr 1 ; Argument tells where to jump when no error.
		test rax, rax
		jns %1
	%endm
%elif OS == "FreeBSD"
	;
	; FreeBSD system calls mostly follow SysV ABI but:
	;   - On success: CF = cleared.
	;   - On error: rax = errno, CF = set.
	;
	%define sys_read 0x03
	%define sys_write 0x04
	%define sys_open 0x05
	%define sys_close 0x06
	%define sys_exit 0x01

	%macro init 0 ; Place arguments { argc, arg0, argv1, ... } in rbp.
		mov rbp, rdi
	%endm

	%macro checkerr 1 ; Argument tells where to jump when no error.
		jnc %1
	%endm
%else
	%fatal "Invalid operating system specified."
%endif

%define stdin  0
%define stdout 1
%define stderr 2

align 16

;
; strlen - macro to find string length
;
; inputs
;   RSI - pointer to string
;
; outputs
;   RDX - result
;
; trashed
;   RAX, RCX, RDI, OF, CF, SF, ZF, PF, AF, DF
;
; side-effects
;   none
;
; notes
;   none
;
%macro strlen 0
	xor rcx, rcx
	dec rcx
	xor al, al
	mov rdi, rsi
	cld
	repne scasb
	not rcx
	dec rcx
	mov rdx, rcx
%endm

;
; print - macro to print 0-terminated string to stdout
;
; inputs
;   RSI - pointer to string
;
; outputs
;   RAX - number of bytes written
;
; trashed
;   RAX, RCX, RDI, OF, CF, SF, ZF, PF, AF, DF
;
; side-effects
;   output to stdout
;
; notes
;   none
;
%macro print 0
	strlen
	mov rdi, stdout
	mov rax, sys_write
	syscall
%endm

section .text

global _start
align 16
_start:
	push rbp
	init
	; Check whether SSE4.2 is supported.
	mov rax, 1
	cpuid
	and ecx, 1 << 20
	jnz near .sse42_ok
	mov rsi, nosse
	print
	jmp .end
.sse42_ok:
	mov rsi, sse
	print
.end:
	xor rdi, rdi
	mov rax, sys_exit
	syscall
	pop rbp
	ret

section .rodata
	sse: db "SSE4.2 supported.", 10, 0
	nosse: db "SSE4.2 not supported according to cpuid.\n", 10, 0

section .bss
	buf: resb BUFSIZE

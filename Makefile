PREFIX = /usr/local
TARGET_OS != uname -s
#AFLAGS += -Fdwarf -g

all: crc32c testcpuid

crc32c: crc32c.asm
	nasm -felf64 $(AFLAGS) -DOS=\"$(TARGET_OS)\" -o $@.o $@.asm
	ld -o $@ $@.o
	brandelf -t $(TARGET_OS) $@

testcpuid: testcpuid.asm
	nasm -felf64 $(AFLAGS) -DOS=\"$(TARGET_OS)\" -o $@.o $@.asm
	ld -o $@ $@.o
	brandelf -t $(TARGET_OS) $@

clean:
	rm -f crc32c testcpuid crc32c.o testcpuid.o

install: crc32c
	install crc32c $(PREFIX)/bin

test: crc32c
	sh ./test.sh
